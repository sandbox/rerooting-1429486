<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
 
global $theme_key;


include_once(dirname(__FILE__) . '/includes/theme.inc');
include_once(dirname(__FILE__) . '/includes/pager.inc');
include_once(dirname(__FILE__) . '/includes/form.inc');
include_once(dirname(__FILE__) . '/includes/menu.inc');



$modules = module_list();

foreach ($modules as $module) {
  if (is_file(drupal_get_path('theme', $theme_key) . '/includes/' . str_replace('_', '-', $module) . '.inc')) {
    include_once(drupal_get_path('theme', $theme_key) . '/includes/' . str_replace('_', '-', $module) . '.inc');
  }    
}

function omega_bootstrap_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];

  if (!empty($breadcrumb)) {
    $breadcrumbs = '<ul class="breadcrumb">';
    
    $count = count($breadcrumb) - 1;
    foreach($breadcrumb as $key => $value) {
      if($count != $key) {
        $breadcrumbs .= '<li>'.$value.'<span class="divider">/</span></li>';
      }else{
        $breadcrumbs .= '<li>'.$value.'</li>';
      }
    }
    $breadcrumbs .= '</ul>';
    
    return $breadcrumbs;
  }
}


function omega_bootstrap_preprocess_page(&$variables) {

// Our custom search because its cool :)
  $variables['search'] = drupal_get_form('_omega_bootstrap_search_form');
  
  // Add user menu
  //$variables['user_menu'] = menu_navigation_links('user-menu');
  }
  
  function _omega_bootstrap_search_form($form, &$form_state) {
  $form = search_form($form, &$form_state);
  $form['#attributes']['class'][] = 'pull-left';
  $form['basic']['keys']['#title'] = '';
  unset($form['basic']['submit']);

  return $form;
}

function omega_bootstrap_preprocess_form_element(&$vars) {
}

